'use strict';
var VideoGrid = angular.module('VideoGallery', ['ngSanitize','videoPlayer']);

VideoGrid.controller('VideoGrid', function($scope){

		$scope.videos = [
			{
				title: 'Earth Video',
				thumb: 'videogular.png',
                keywords:'demo, science, earth',
                description: 'This is a demo video that I want to test things out in.',
                target: '0'
			},
            {
                title: 'Percentages',
                thumb: '02_05_03_percentages.jpg',
                keywords:'math, percentages, basketball',
                description: 'This is a math video.',
                target: '1'
            },
            {
                title: 'Carbon Lab',
                thumb: 'carbon_lab.jpg',
                keywords:'carbon, science, lab',
                description: 'this is a cool video about carbon. This was used in a science POC',
                target: '2'
            },
            {
                title: 'Ethics',
                thumb: 'ethics.png',
                keywords:'ethics, journalism, english',
                description: 'Learn all about ethics in Journalism.',
                target: '3'
            },
            {
                title: 'The Rebuttal',
                thumb: 'rebuttal.png',
                keywords:'rebuttal, journalism, point, counter-point',
                description: 'You will learn how to have the perfect rebuttal... not like Homer Simpson.',
                target: '4'
            },
            {
                title: 'Microsoft Office Ribbon',
                thumb: 'ribbondisplay.png',
                keywords:'office, online, microsoft, ribbon',
                description: 'Here is how you can access the Ribbon in Microsoft Office Online.',
                target: '5'
            }
		];

});

