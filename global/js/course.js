// ----------------------------------------------------------------------
// -- COURSE SPECIFI JS:
// ----------------------------------------------------------------------
// -- NOTE: This is where you can add anything you need to do specifically to the course, it will load lastly.
// -- ABOUT: THis file will over-ride everything else, if you need to customize
// -- AUTHOR: You - WDS
// ======================================================================
/* indexOf for older browsers */
if (!Array.indexOf) {
    Array.prototype.indexOf = function (obj, start) {
        for (var i = (start || 0); i < this.length; i++) {
            if (this[i] == obj) {
                return i;
            }
        }
        return -1;
    }
}
/* end indexOf */

$(document).ready(function(){

    //on resize change viewport viewer
    //window.addEventListener("resize", function() {
    //    $('#viewport').empty().append($(window).width(), "x", $(window).height());
    //});


    // Code for changing link of the top-right Home button, written by FLVS
    //home_url = "0"+FLVS.Sitemap.module[current_module].title.substring(0,1)+"_01_01.htm";
    //$('.link_home').attr('href',home_url);


    if(typeof isexternal === 'undefined' && typeof islearningobject === 'undefined'){

        // Populate Breadcrumb
        $('.breadcrumbs_course').html(FLVS.settings.course_title);

        // Replace module title with "MODULE #"
        var modTitle = FLVS.Sitemap.module[current_module].title;
        //if(FLVS.Sitemap.module[current_module].title.substring(0,1) == "G"){	// Original FLVS nav code commented out to solve breadcrumb issue for POCs

        //	$('.breadcrumbs_module').html(": GETTING STARTED");
        //	$('.breadcrumbs_lesson').html("");

        //}else{

        //	$('.breadcrumbs_module').html(": MODULE "+FLVS.Sitemap.module[current_module].title.substring(0,1));
        //	$('.breadcrumbs_lesson').html(": "+FLVS.Sitemap.module[current_module].title.substring(4));

        //}

        pageNum = $('.pagenum').eq(0).text();    // Lines 46-58 written by Kevin Switzer to solve breadcrumbs issue for Toolwire POC
        pageNum = pageNum.substr(0,pageNum.length-2);   //Lines 49-61 edited/added by Hugo

        var lessonNum = "";
        try{
            //lessonNum = ": LESSON " + parseInt(FLVS.Sitemap.module[current_module].lesson[0].num.split(".")[1]);
            lessonNum = " LESSON " + current_lesson;
        }catch (err){
        }
        if(FLVS.Sitemap.module[current_module].title.substring(0,1) == "G"){

            $('.breadcrumbs_module').html(": GETTING STARTED");
            $('.breadcrumbs_lesson').html("PAGE "+pageNum);
        }else{
            $('.breadcrumbs_module').html(" MODULE "+FLVS.Sitemap.module[current_module].title);
            $('.breadcrumbs_lesson').html(+FLVS.Sitemap.module[current_module].lesson[current_lesson].num + " " + FLVS.Sitemap.module[current_module].lesson[current_lesson].title);
        }


        // FADE IN CONTENT and position the nav_menu
        //$('body').css('visibility', 'visible').hide();
        //$('body').fadeIn(800, function () {

            // Navigation Position
            var pos = $('#menu_inner').offset();
            $('#nav_menu').css('left', pos.left + 'px');

            // Position Popup Menu
            $(window).on('resize', function () {
                var pos = $('#menu_inner').offset();
                $('#nav_menu').css('left', pos.left + 'px');

            });
        //});

// Create Popup Menu
        createMenu();

        // Event for Menu Button
        $('.menubtn, .menubtn_mobile').click(function () {
            $('.nav_menu_lessons').hide();

            if (!$('#nav_menu').is(':visible')) {
                $('body').append('<div class="menu_backdrop">&nbsp;</div>');
                $('.menu_backdrop').click(function () {
                    $('#nav_menu').fadeToggle('fast');
                    $(this).remove();
                });
            } else {
                $('.menu_backdrop').remove();
            }

            $('#nav_menu').fadeToggle('fast');

        });

        // Event for Showing Menu Lessons
        $('.modlink').click(function () {
            $('.nav_menu_lessons').hide();
            $(this).next().stop().fadeIn('fast');
        });
        function createMenu() {
            var menu = '<ul class="nav_menu_modules">';
            for (var i = 0; i < FLVS.Sitemap.module.length; i++) {
                if (FLVS.Sitemap.module[i].title.substring(0, 1) == FLVS.Sitemap.module[current_module].title.substring(0, 1)) {
                    menu += '<li>';
                    menu += '<a href="javascript:void(0);" class="modlink">' + FLVS.Sitemap.module[i].title + '</a>';
                    //menu += '<a href="javascript:void(0);" class="modlink"><span class="modnum">MOD '+(i+1)+'</span>'+FLVS.Sitemap.module[i].title;+'</a>';
                    // Lessons
                    menu += '<ul class="nav_menu_lessons mod' + (i + 1) + '">';
                    var submenu = '';
                    if (typeof FLVS.Sitemap.module[i].lesson != 'undefined') { // Added to account for POC process where not all lessons in rest of course have content
                        for (var j = 0; j < FLVS.Sitemap.module[i].lesson.length; j++) {
                            var link = FLVS.Sitemap.module[i].lesson[j].page[0].href;

                            submenu += '<li>';
                            submenu += '<a href="' + link + '"><span class="lesson_num">' + FLVS.Sitemap.module[i].lesson[j].num.substring(1) + '</span>';


                            var minutes = "mins";
                            if (Number(FLVS.Sitemap.module[i].lesson[j].time) < 2) {
                                minutes = "min";
                            }
                            var points = "pts";
                            if (Number(FLVS.Sitemap.module[i].lesson[j].points) < 2) {
                                points = "pt";
                            }

                            submenu += '<span class="lesson_title">' + FLVS.Sitemap.module[i].lesson[j].title + '</span></a>';
                            submenu += '</li>';
                        }
                    }
                    menu += submenu;
                    menu += '</ul>';

                    menu += '</li>';
                }
            }
            menu += '</ul>';


        }

// Remove all modlinks from nav_menu_lessons
        $('#nav_menu').append(menu);
        $('.nav_menu_lessons .modlink').remove();

    }

    // Puts the image caption inside of a block below the image.
    $('.image-container').each(function(){
        var imgsize = $(this).find('img').width();
        $(this).find('span.copyright').css('max-width',(imgsize - 30) + 'px');
        $(this).find('span.caption').css('max-width', (imgsize - 30) + 'px');
    });

    if($('.landingImage').length != 0){
        sizeLandingPage();
    }


    if($('.prevt').attr('href').search('index') != -1){	// Back button fix for Toolwire delivery
        $('.prevt').attr('href',home_url);
    }

    if($('.nextt').attr('href').search('index') != -1){	// Forward button fix for Toolwire delivery
        $('.nextt').attr('href',home_url);
    }

});

function sizeLandingPage(){
    var lessonList = createLessonList(FLVS.Sitemap.module[current_module].title.substring(0,1));
    $('.lessonContainerHeader').after(lessonList);
}

function createLessonList(i){
    var menu = '<ul class="lessonLists">';
    var module = FLVS.Sitemap.module[i];

    for(var j=1; j < module.lesson.length; j++){
        var links = module.lesson[j].page[0].href;
        links = links.substr(3,999);

        menu += '<li><a href="../' + links + '">';
        menu += '<span class="lesson_num">' + module.lesson[j].num + '</span>';
        menu += '<span class="lesson_title">' + module.lesson[j].title + '</span>';
        menu += '<span class="lesson_time">' + module.lesson[j].time + ' mins</span>';
        menu += '<span class="lesson_points">' + module.lesson[j].points + '</span>';
        menu += '</a></li>';
    }

    if (i == 4){
        menu += '<li><span class="lesson_num">04.07</span>';
        menu += '<span class="lesson_title">Segment One Collaboration</span>';
        menu += '<span class="lesson_time">90 mins</span>';
        menu += '<span class="lesson_points">40</span></li>';

        menu += '<li><span class="lesson_num">04.08</span>';
        menu += '<span class="lesson_title">Segment One Exam</span>';
        menu += '<span class="lesson_time">45 mins</span>';
        menu += '<span class="lesson_points">165</span></li>';
    }
    else if (i == 8){
        menu += '<li><span class="lesson_num">08.07</span>';
        menu += '<span class="lesson_title">Segment Two Collaboration</span>';
        menu += '<span class="lesson_time">90 mins</span>';
        menu += '<span class="lesson_points">40</span></li>';

        menu += '<li><span class="lesson_num">08.08</span>';
        menu += '<span class="lesson_title">Segment Two Exam</span>';
        menu += '<span class="lesson_time">45 mins</span>';
        menu += '<span class="lesson_points">157</span></li>';
    }


    menu += '</ul>';

    return menu;
}


$('*').click(function (e) {
    $('#navSubmenu').css({'visibility': 'hidden'});
});

$('li').click(function (e) {
    e.stopPropagation();
    clickedDiv = '#' + $(this).attr('id');

    $('#navSubmenu').css({'visibility': 'hidden'});

    navList = ['#nav01', '#nav02', '#nav03', '#nav04', '#nav05', '#nav06', '#nav07', '#nav08',]

    if (jQuery.inArray(clickedDiv, navList) >= 0) {
        modNum = clickedDiv.substring(5);
        midPoint = (($(clickedDiv).position().left + $(clickedDiv).width()) + $(clickedDiv).position().left) / 2;
        borderWidth = ($(document).width() - $('.navCircles').width()) / 2;
        bottomPoint = ($(window).height() - $(clickedDiv).offset().top);
        leftPoint = $(clickedDiv).position().left
        menu = createLessons(modNum);

        if (($(document).width() - leftPoint) < $('#navSubmenu').width()) {
            $('#navSubmenu').css('background-image', 'url(global/images/home/submenu_bottom_right.png)');
            leftPoint = leftPoint - 120;
        } else {
            $('#navSubmenu').css('background-image', 'url(global/images/home/submenu_bottom_left.png)');
        }

        $('.triangle-right').html(menu);

        finalHeight = $('.triangle-right').height() + 40;

        //throw "stop execution";

        $('#navSubmenu')
            .css(
            {'bottom': 120, 'left': leftPoint, 'height': 0, 'opacity': 0, 'visibility': 'visible'}
        )
            .animate(
            {'height': finalHeight, opacity: 1, queue: false, duration: 'fast'}, function () {
                doBounce($('#navSubmenu'), bottomPoint, 15, 80);
            }
        );
    }
});

function doBounce(element, fromPoint, distance, speed) {
    while (distance > 0) {
        element.animate({bottom: '-=' + distance + 'px'}, speed)
            .animate({bottom: '+=' + distance + 'px'}, speed);

        distance = Math.floor(distance / 2);
        speed = Math.floor(speed / 2);

    }
}

$(window).resize(function() {
    if($('.landingImage').length != 0){
        //sizeLandingPage();
    }
});