'use strict';
var VideoGrid = angular.module('videoPlayer',
    [
        'ngSanitize',
        'com.2fdevs.videogular',
        'com.2fdevs.videogular.plugins.controls',
        'com.2fdevs.videogular.plugins.overlayplay',
        'com.2fdevs.videogular.plugins.poster',
        'com.2fdevs.videogular.plugins.buffering'
    ]
)
    .controller('HomeCtrl',
    ["$sce", "$timeout", function ($sce, $timeout) {
        var controller = this;
        controller.state = null;
        controller.API = null;
        controller.currentVideo = 0;

        controller.onPlayerReady = function(API) {
            controller.API = API;
        };

        controller.onCompleteVideo = function() {
            controller.isCompleted = true;

            controller.currentVideo++;

            if (controller.currentVideo >= controller.videos.length) controller.currentVideo = 0;

            controller.setVideo(controller.currentVideo);
        };

        controller.videos = [
            {
                sources: [
                    {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.mp4"), type: "video/mp4"},
                    {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm"},
                    {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg"}
                ]
            },
            {
                sources: [
                    {src: $sce.trustAsResourceUrl("videos/02_05_03_percentages.mp4"), type: "video/mp4"}

                ]
            },
            {
                sources: [
                    {src: $sce.trustAsResourceUrl("videos/carbon_lab.mp4"), type: "video/mp4"}

                ]
            },
            {
                sources: [
                    {src: $sce.trustAsResourceUrl("videos/ethics.mp4"), type: "video/mp4"}

                ]
            },
            {
                sources: [
                    {src: $sce.trustAsResourceUrl("videos/rebuttal.mp4"), type: "video/mp4"}

                ]
            },
            {
                sources: [
                    {src: $sce.trustAsResourceUrl("videos/RibbonDisplay_letterbox.mp4"), type: "video/mp4"}

                ]
            }
        ];

        controller.config = {
            preload: "none",
            autoHide: false,
            autoHideTime: 3000,
            autoPlay: false,
            sources: controller.videos[0].sources,
            theme: {
                url: "css/videogular.css"
            },
            plugins: {
                poster: "imgs/videogular.png"
            }
        };

        controller.setVideo = function(index) {
            controller.API.stop();
            controller.currentVideo = index;
            controller.config.sources = controller.videos[index].sources;
            $timeout(controller.API.play.bind(controller.API), 100);
        };
    }]
);